﻿Shader "CustomShaders/NormalMap" {
	Properties {
		_MainTint("Diffuse Tint", Color) = (1,1,1,1)
		_NormalTex("Normal Map", 2D) = "bump" {}
		_NormalIntensity("Normal Map Intensity", Range(0, 2)) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _NormalTex;
		float4 _MainTint;
		float _NormalIntensity;

		struct Input {
			float2 uv_NormalTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			// Get normal data out of texture with built in UnpackNormal function
			float3 normalMap = UnpackNormal(tex2D(_NormalTex, IN.uv_NormalTex));
			normalMap = float3(normalMap.x * _NormalIntensity, normalMap.y * _NormalIntensity, normalMap.z * _NormalIntensity);
			
			o.Normal = normalMap.rgb;
			o.Albedo = _MainTint.rgb;
			o.Alpha = _MainTint.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
