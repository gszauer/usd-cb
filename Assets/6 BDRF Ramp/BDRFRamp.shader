﻿Shader "CustomShaders/BDRFRamp" {
	Properties {
		_EmmisiveColor("Emissive Color", Color) = (1,1,1,1)
		_AmbientColor("Ambient Color", Color) = (1,1,1,1)
		_Power("Power", Range(0, 10)) = 2.5
		_RampTex ("Ramp Texture", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Tell the shader which lighting model to use
		#pragma surface surf DiffuseModel

		float4 _EmmisiveColor;
		float4 _AmbientColor;
		float _Power;
		sampler2D _RampTex;
		
		// Custom lighting model name: Lighting<ModelName>
		// Forward rendering when view direction is not needed
		// 		half4 LightingName (SurfaceOutput s, half3 lightDir, half atten)
		// Forward rendering when view direction is needed
		//		half4 LightingName (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		// Used for defered rendering
		//		half4 LightingName_PrePass (SurfaceOutput s, half4 light)
		inline float4 LightingDiffuseModel(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten) {
			float diffLight = dot(s.Normal, lightDir);
			float rimLight = dot(s.Normal, viewDir);
			float halfLambert = diffLight * 0.5 + 0.5;
			float3 ramp = tex2D(_RampTex, float2(halfLambert, rimLight)).rgb;
			
			float4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * ramp;
			c.a = s.Alpha;
			return c;
		}
		
		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			float4 c = pow((_EmmisiveColor + _AmbientColor), _Power);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
