﻿Shader "CustomShaders/AnimatedSprites" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TexWidth("Texture Width", float) = 0.0
		_CellAmmount("Cell Ammount", float) = 0.0
		_Speed("Speed", Range(0.1, 32)) = 12
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		
		float _TexWidth;
		float _CellAmmount;
		float _Speed;
		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			float2 spriteUv = IN.uv_MainTex;
			
			float cellPixelWidth = _TexWidth / _CellAmmount;
			float cellUvPrecentage = cellPixelWidth / _TexWidth;
			float timeVal = ceil(fmod(_Time.y * _Speed, _CellAmmount));
			
			float xVal = spriteUv.x;
			xVal += cellUvPrecentage * timeVal * _CellAmmount;
			xVal *= cellUvPrecentage;
			
			spriteUv = float2(xVal, spriteUv.y);
			
			half4 c = tex2D (_MainTex, spriteUv);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
