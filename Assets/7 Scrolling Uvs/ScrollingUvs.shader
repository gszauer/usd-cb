﻿Shader "CustomShaders/ScrollingUvs" {
	Properties {
		_MainTint("Diffuse Tint", Color) = (1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_XScrollSpeed("X Scroll Speed", Range(0, 10)) = 2
		_YScrollSpeed("Y Scroll Speed", Range(0, 10)) = 2
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		
		float4 _MainTint;
		fixed _XScrollSpeed;
		fixed _YScrollSpeed;
		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed2 scrollUv = IN.uv_MainTex;
			fixed xScroll = _XScrollSpeed * _Time;
			fixed yScroll = _YScrollSpeed * _Time;
			scrollUv += fixed2(xScroll, yScroll);
			
			half4 c = tex2D (_MainTex, scrollUv);
			o.Albedo = c.rgb * _MainTint.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
